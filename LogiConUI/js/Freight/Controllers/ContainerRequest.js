﻿angular.module('LogiCon').controller('ContainerRequestCntrl', ['$scope', '$uibModal', 'Utility', 'MerchantProfileService', 'VesselScheduleService', 'limitToFilter', 'UtilityFunc', 'growlService',
    function ($scope, $uibModal, Utility, MerchantProfileService, VesselScheduleService, limitToFilter, UtilityFunc, growlService) {

        $scope.init = function () {
            $scope.DateFormat = UtilityFunc.DateFormat();
            $scope.cr = {
                containerHeader: {
                    RequestNo: null,
                    HaulierCode: null,
                    HaulierName: null,
                    JobType: null,
                    PickupFrom: null,
                    DropOffCode: null,
                    RequestDate: new Date(),
                    RequestBy: null,
                    OrderNo: null,
                    Remark: null,
                    ROTNo: null,
                    BillToCode: null,
                    BillToName: null,
                    IsBillable: null,
                    IsCancel: null,
                    CancelBy: null,
                    CancelOn: null,
                    IsApproved: null,
                    ApprovedBy: null,
                    ApprovedOn: null,
                    CreatedBy: null,
                    CreatedOn: null,
                    ModifiedBy: null,
                    ModifiedOn: null
                },
                containerDetails: new Array()
            };

        };

        var conIndex = -1;
        $scope.AddContainerRequest = function (index) {
            conIndex = index;
            $scope.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Freight/Templates/ContainerRequest/add-container-request.html?v=' + Utility.Version,
                controller: 'addContainerRequestCntrl',
                //size: 'lg',
                windowClass: 'app-modal-window'

            });

            $scope.modalInstance.result.then(function (res) {
                debugger;
                if (conIndex != -1) 
                    $scope.cr.containerDetails[conIndex] = res;
                else
                    $scope.cr.containerDetails.push(res);
            }, function () {

            });
        };

        $scope.GenericMerchantResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.MerchantSelected = function (merchantObject, Type) {
            $scope.cr[Type] = merchantObject.Value;
        };
        $scope.vesselName = {};
        $scope.VesselNameResults2 = function (text) {

            return VesselScheduleService.VesselNameResults(text).then(function (d) {

                $scope.vesselName = d.data.vesselList;
                //return limitToFilter(d.data.vesselList, 15);
            }, function (err) { });
        };

        $scope.isFromValid = false;
        $scope.$watch('frmContainerRequest.$valid', function (isValid) {
            $scope.isFromValid = isValid;
        });

        $scope.SaveConRequest = function (obj) {
            debugger;
            if ($scope.isFromValid) { }
            else {
                growlService.growl('Please enter all mandatory fields', 'danger');
            }
        }

        $scope.init();
    }]);
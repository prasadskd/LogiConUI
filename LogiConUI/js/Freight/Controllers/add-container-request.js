﻿angular.module('LogiCon').controller('addContainerRequestCntrl', ['$scope', '$uibModalInstance', 'growlService', function ($scope, $uibModalInstance, growlService) {

    $scope.init = function () {
        //$scope.con = {
        //    RequestNo: null,
        //    ContainerKey: null,
        //    Remarks: null,
        //    IsCancel: null,
        //    CreatedBy: null,
        //    CreatedOn: null,
        //    ModifiedBy: null,
        //    ModifiedOn: null
        //}
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.isFrmValid = false;
    $scope.$watch('frmContainerInfo.$valid', function (isValid) {
        $scope.isFrmValid = isValid;
    });


    $scope.SaveContainer = function (obj) {
        if ($scope.isFrmValid) {
            $uibModalInstance.close(obj);
        }
        else
            growlService.growl('Please enter all mandatory fields', 'danger');
    }
}]);
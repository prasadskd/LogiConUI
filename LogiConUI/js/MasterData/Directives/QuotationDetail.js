﻿app.directive('quotationDetail', [function () {
    
    return {
        restrict: 'E',
        replace: true,
        scope: {
            details: '=data'
        },
        templateUrl: 'Js/MasterData/Directives/quotation-detail.html',
        controller: ['$scope', 'CustomerQuotationService', 'ChargeCodeService', 'OrderEntryService', 'limitToFilter', '$q', '$http','Utility',
            function ($scope, CustomerQuotationService, ChargeCodeService, OrderEntryService, limitToFilter, $q, $http, Utility) {
                $scope.isfrmQuotationDetailsValid = false;
                $scope.$watch('frmQuotationDetails.$valid', function (isValid) {
                    $scope.isfrmQuotationDetailsValid = isValid;
                });

                $scope.lookupData = {};
                $scope.getLookUpData = function () {

                    var lookupPromise = CustomerQuotationService.GetLookupData('quotationdetail');
                    if ($scope.details.Size == null)
                        $scope.details.Size = "20";
                    var sizeTypePromise = OrderEntryService.GetSizeType($scope.details.Size);

                    $q.all([lookupPromise, sizeTypePromise]).then(function (d) {
                        $scope.lookupData = d[0].data;
                        $scope.lookupData.TypeList = d[1].data;
                    }, function (err) { })
                };

                $scope.ChargeCodeResults = function (text) {
                    return ChargeCodeService.GetChargeCodeSearch(text).then(function (response) {
                        return limitToFilter(response.data, 15);
                    }, function (err) { });
                };

                $scope.SaveQuotationDetail = function (details) {
                    if ($scope.isfrmQuotationDetailsValid) {
                        $scope.$parent.addDetails(details);
                    } else {

                    }
                };
                $scope.requiredValue = false;
                $scope.AddRequired = function () {
                    if ($scope.details.IsSlabRate)
                        $scope.requiredValue = true;
                    else
                        $scope.requiredValue = false;
                }
                $scope.cancel = function () {
                    $scope.$parent.cancel();
                };

                $scope.sizeChanged = function () {
                    OrderEntryService.GetSizeType($scope.details.Size).then(function (d) {
                        
                        $scope.lookupData.TypeList = d.data;
                    }, function () { })
                };

                $scope.OnChargeCodeSelect = function (item) {
                    $scope.GetChargeCodeGSTValues(item.Value);
                };
                $scope.GetChargeCodeGSTValues = function (chargeCode) {
                    ChargeCodeService.GetGSTValues(chargeCode).then(function (d) {
                        $scope.details.ChargeCode = chargeCode;
                        if (!angular.isUndefined(d.data.outputGSTCodeDetails) && d.data.outputGSTCodeDetails != null && d.data.outputGSTCodeDetails.length > 0)
                            $scope.details.OutPutGSTCodeDescription = d.data.outputGSTCodeDetails[0].OutPutGSTCodeDescription;
                        if (!angular.isUndefined(d.data.inputGSTCodeDetails) && d.data.inputGSTCodeDetails != null && d.data.inputGSTCodeDetails.length > 0)
                            $scope.details.InputGSTCodeDescription = d.data.inputGSTCodeDetails[0].InputGSTCodeDescription;
                    }, function (err) { });
                };

                $scope.GenericMerchantResults = function (text, filter, addresstype) {
                    return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + text + '/' + filter).then(function (response) {
                        if (response.data.length == 0)
                            $scope.oe[addresstype] = '';
                        return limitToFilter(response.data, 15);
                    });
                };

                $scope.CustomerSelected = function (item, type) {
                    $scope.details[type] = item.Value;
                };

                $scope.getLookUpData();

                if ($scope.details.ChargeCode != null && $scope.details.ChargeCode != '')
                    $scope.GetChargeCodeGSTValues($scope.details.ChargeCode);
            }],
        link: function ($scope, element, attrs) {
            //$scope.isextra = attrs.quotation == 'standard' ? true : false;
        }
    };
}]);

/*
app.directive('quotationDetail', function ($uibModalInstance) {
    
    return {
        restrict: 'E',
        replace: true,
        scope: {
            
        },
        templateUrl: 'Js/Tms/Directives/quotation-detail.html',
        controller: ['$scope', 'quotationDetailsObj', function ($scope, quotationDetailsObj) {
            
        }],
        link: function ($scope, element, attrs) {
            
        }
    };
});
*/

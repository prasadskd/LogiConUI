﻿angular.module('LogiCon').controller('CustomerQuotationCntrl', ['$scope', '$uibModal', 'CustomerQuotationService', 'limitToFilter', '$stateParams', '$location', 'MerchantProfileService', '$filter', 'growlService', 'Utility', 'UtilityFunc', '$state',
    function ($scope, $uibModal, CustomerQuotationService, limitToFilter, $stateParams, $location, MerchantProfileService, $filter, growlService, Utility, UtilityFunc, $state) {
        $scope.tabs = [
           { title: 'Customer Quotation', content: 'Js/MasterData/Templates/CustomerQuotation/general.html?v=' + Utility.Version, active: true, disabled: false },
           { title: 'Details', content: 'Js/MasterData/Templates/CustomerQuotation/details.html?v=' + Utility.Version, active: false, disabled: false }
        ];
        $scope.dateFormat = UtilityFunc.DateFormat();

        $scope.cq = {
            QuotationItems: new Array()
        };
        $scope.cq.QuotationDate = moment();
        //$scope.cq.EffectiveDate = moment();
        $scope.mindate = moment();
        $scope.AddCQDetails = function (index) {
            var modalInstance = $uibModal.open({
                animation: true,
                template: '<quotation-detail data="data" quotation="customer"></quotation-detail>',
                controller: 'QuotationDetailCntrl',
                windowClass: 'app-modal-window4',
                resolve: {
                    quotationDetailsObj: function () {
                        
                        return (index != -1 ? $scope.cq.QuotationItems[index] : { Index: index });
                    }
                }
            });

            modalInstance.result.then(function (quotationDetails) {
                
                if ($scope.cq.QuotationItems != null) {
                    if (quotationDetails.Index != -1) {
                        $scope.cq.QuotationItems[quotationDetails.Index] = quotationDetails;
                    }
                    else {
                        $scope.cq.QuotationItems.push(quotationDetails);
                    }
                }
                else {
                    $scope.cq.QuotationItems = new Array();
                    $scope.cq.QuotationItems.push(quotationDetails);
                }
            }, function () {

            });
        };

        $scope.deleteCQDetails = function (index) {
            
            $scope.cq.QuotationItems.splice(index, 1);
        };
        $scope.isFrmCustomerQuotationValid = false;
        $scope.$watch('frmCustomerQuotation.$valid', function (valid) {
            $scope.isFrmCustomerQuotationValid = valid;
        });

        $scope.SaveCustomerQuotation = function (cq) {
            if ($scope.isFrmCustomerQuotationValid) {
                CustomerQuotationService.SaveCustomerQuotation(cq).then(function (d) {
                    
                    growlService.growl(d.data.message, 'success');
                    //$location.reload();
                    $state.go('customerquotation', {
                        quotation: d.data.quotation
                    });

                }, function (err) {
                    
                    growlService.growl('A QUOTATION ALREADY EXISTS WITH THIS DATE RANGE', 'danger');
                });
            } else {
                growlService.growl('Please enter all mandatory fields..', 'danger');
            }
        };

        $scope.lookupData = {};
        $scope.getLookUpData = function () {
            CustomerQuotationService.GetLookupData('quotation').then(function (d) {
                $scope.lookupData = d.data;
            }, function (err) { });
        };

        $scope.getLookUpData();

        $scope.CustomerResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (response) {
                return limitToFilter(response.data, 15);
            }, function (err) { });
        };

        $scope.AgentResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (response) {
                return limitToFilter(response.data, 15);
            }, function (err) { });
        };

        $scope.ForwarderResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (response) {
                return limitToFilter(response.data, 15);
            }, function (err) { });
        };

        $scope.CustomerQuotationList = function () {
            CustomerQuotationService.getCustomerQuotationListByType(3861).then(function (d) {
                $scope.quotationlist = d.data;
            }, function () { });
        };

        var quotationNo = $stateParams.quotation;
        if (typeof quotationNo != 'undefined' && quotationNo != 'NEW' && quotationNo != '') {
            
            CustomerQuotationService.getCustomerQuotation(quotationNo).then(function (d) {
                $scope.cq = d.data;

                if ($scope.cq.QuotationDate != null) {
                    $scope.cq.QuotationDate = new Date(moment($scope.cq.QuotationDate));
                }

                if ($scope.cq.EffectiveDate != null)
                    $scope.cq.EffectiveDate = new Date(moment($scope.cq.EffectiveDate));

                if ($scope.cq.ExpiryDate != null)
                    $scope.cq.ExpiryDate = new Date(moment($scope.cq.ExpiryDate));
            }, function (err) { });
        }
        else {
            $scope.CustomerQuotationList();
        }

        $scope.backToList = function () {
            $location.path('/MasterData/customerquotation/list');
        };

        $scope.searchClick = function (type) {
            $stateParams.code = $scope.cq[type];
        };

        $scope.AddQuotation = function (quotationNo) {
            $location.path('/MasterData/customerquotation/' + quotationNo);
        }


        $scope.DeleteQuotation = function (quotationNo) {
            
            CustomerQuotationService.DeleteQuotation(quotationNo).then(function (d) {
                
                growlService.growl('Deleted successfully', 'success');
                $scope.CustomerQuotationList();
            }, function (err) { });
        };

        $scope.validateEffectivedate = function () {
            if (!angular.isUndefined($scope.cq.EffectiveDate) && angular.isUndefined($scope.cq.ExpiryDate)) {
                $scope.cq.ExpiryDate = moment($scope.cq.EffectiveDate).add(30, 'days');
            }
            if ($scope.cq.ExpiryDate != undefined && $scope.cq.EffectiveDate != undefined) {
                var effectiveDate = moment($scope.cq.EffectiveDate);
                var expiryDate = moment($scope.cq.ExpiryDate);
                if (effectiveDate == expiryDate || effectiveDate > expiryDate) {
                    $scope.cq.ExpiryDate = undefined;
                    growlService.growl('ExpiryDate Should be greater than Effective date', 'danger');
                }
            }
        }
    }]);




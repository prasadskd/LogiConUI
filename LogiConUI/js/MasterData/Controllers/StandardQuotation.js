﻿angular.module('LogiCon').controller('StandardQuotationCntrl', ['$scope', '$uibModal', 'CustomerQuotationService', 'UtilityFunc','growlService',
    function ($scope, $uibModal, CustomerQuotationService, UtilityFunc, growlService) {

        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
        $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();
        //$scope.maxdate = moment();
        $scope.mindate = moment();
        
        $scope.IsfrmStandardCntrl = false;
        $scope.cq = {
            QuotationItems: new Array()
        };
        $scope.cq.QuotationNo = UtilityFunc.StandardQuotationkey();
        $scope.cq.QuotationDate = moment();
        
        $scope.$watch('frmStandardCntrl.$valid', function (valid) {
            $scope.IsfrmStandardCntrl = valid;
        });

        $scope.deleteCQDetails = function (index) {
            $scope.cq.QuotationItems.splice(index,1);
        }

        $scope.AddCQDetails = function (index) {
            var modalInstance = $uibModal.open({
                animation: true,
                template: '<quotation-detail data="data" quotation="customer"></quotation-detail>',
                controller: 'QuotationDetailCntrl',
                windowClass: 'app-modal-window4',
                resolve: {
                    quotationDetailsObj: function () {
                        return (index != -1 ? $scope.cq.QuotationItems[index] : { Index: index });
                    }
                }
            });

            modalInstance.result.then(function (quotationDetails) {
                
                if ($scope.cq.QuotationItems != null) {
                    if (quotationDetails.Index != -1) {
                        $scope.cq.QuotationItems[quotationDetails.Index] = quotationDetails;
                    }
                    else {
                        $scope.cq.QuotationItems.push(quotationDetails);
                    }
                }
                else {
                    $scope.cq.QuotationItems = new Array();
                    $scope.cq.QuotationItems.push(quotationDetails);
                }
            }, function () {

            });
        };

        $scope.SaveStndQuotation = function (cq) {
            
            if ($scope.IsfrmStandardCntrl) {
                
                if ($scope.cq.QuotationItems.length > 0) {
                    CustomerQuotationService.SaveCustomerQuotation(cq).then(function (d) {
                        
                        growlService.growl(d.data.message, 'success');
                        $state.go('standardquotation', {
                            quotation: d.data.quotation
                        });
                    }, function (err) { });
                }
                else{
                    growlService.growl('Please add atleast one quotation detail', 'success');
                }
            }
            
        };

        $scope.getLookUpData = function () {
            CustomerQuotationService.GetLookupData('quotation').then(function (d) {
                $scope.lookupData = d.data;
            }, function (err) { });
        };

        $scope.getLookUpData();
       
        $scope.GetStandardQuotaion = function ()
        {
            CustomerQuotationService.GetStandardQuotation().then(function (d) {
                
                $scope.cq = d.data;
            }, function (err) {
                
            });
        }
        $scope.GetStandardQuotaion();
    }]);


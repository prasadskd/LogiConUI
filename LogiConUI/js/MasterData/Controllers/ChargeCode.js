﻿angular.module('LogiCon').controller('ChargeCodeController', ['$scope', '$stateParams', 'ChargeCodeService', 'GstRateService', '$location', '$window', 'growlService', 'Utility', '$state','NgTableParams',
    function ($scope, $stateParams, ChargeCodeService, GstRateService, $location, $window, growlService, Utility, $state, NgTableParams) {
   
        var DataTblobj = {};
        $scope.isNew = true;
    $scope.GetTableData = function () {
        $scope.ngTblData = new NgTableParams({
        page: 0,
        count: 10,
        sorting: {
            CreatedOn: 'desc'
        }
    }, {
            counts: [10, 20, 30],
            getData: function ($defer, params) {

                DataTblobj.offset = params.page() == 0 ? 0 : (params.count() * (params.page() - 1));
                DataTblobj.limit = params.count();
                if (params.sorting()) {
                    var orderBy = params.orderBy()[0];

                    DataTblobj.sortColumn = orderBy.substring(1);
                    DataTblobj.sortType = orderBy[0] == '+' ? 'asc' : 'desc'
                }

                ChargeCodeService.GetTableList(DataTblobj).then(function (res) {                    
                    params.total(res.data.records);
                    $defer.resolve(res.data.data);
                }, function (err) { })
            }
        });
    };

    $scope.cc = {};    
    $scope.SaveChargeCode = function (cc) {
        
        if ($scope.isfrmChargeCodeValid) {
            ChargeCodeService.SaveChargeCode(cc).then(function (d) {
                
                growlService.growl(d.data, 'success');
                $scope.isNew = false;
                $state.go('chargecodemaster', {});

               // $location.path('/masterdata/chargecodelist');
            }, function () { });
        } else {
            growlService.growl('Please enter all mandatory fields..', 'danger');
        }
    };

    $scope.AddChargeCode = function (chargeCode) {
        $location.path('/masterdata/chargecodemaster/' + chargeCode);
    };

    $scope.navToList = function () {
        $state.go('chargecodelist', {});
    };
    var chargeCode = $stateParams.chargecode;    
    $scope.showLoading = true;
    if (typeof chargeCode != 'undefined') {
        if (chargeCode != 'NEW' && chargeCode != '') {
            ChargeCodeService.GetChargeCode(chargeCode).then(function (d) {                
                $scope.cc = d.data;
                $scope.isNew = false;
                $scope.truefalse = false;
            }, function () { });
        }

        ChargeCodeService.GetLookupData().then(function (d) {
            $scope.showLoading = false;
            $scope.lookupData = d.data;
            $scope.truefalse = true;
        }, function () { });
    }    
    else {        
        $scope.GetTableData();
    }

    $scope.isChargeCodeExists = function (chargeCode) {
        
        if (chargeCode != '') {
            ChargeCodeService.GetChargeCode(chargeCode).then(function (d) {
                
                if (d.data != null) {
                   
                    growlService.growl('Charge Code already exists', 'warning');
                    $scope.cc.ChargeCode = '';
                }

            }, function () { });
        }
    };

    $scope.isfrmChargeCodeValid = false;
    $scope.$watch('frmChargeCode.$valid', function (isValid) {
        $scope.isfrmChargeCodeValid = isValid;
    });

    $scope.gstChange = function (gstRate, gstCode) {
        GstRateService.GetRateByGstCode($scope.cc[gstCode]).then(function (d) {
            $scope.cc[gstRate] = d.data.Rate.toFixed(2);
        }, function (err) { });
    };
       
    $scope.DeleteChargeCode = function (chargeCode) {
        if ($window.confirm('Are you sure, you want to delete \'' + chargeCode + '\' ?')) {
            ChargeCodeService.DeleteChargeCode(chargeCode).then(function (d) {
                growlService.growl('Deleted successfully', 'success');
                $scope.GetTableData();
            }, function (err) { });
        }
    };

  

}]);



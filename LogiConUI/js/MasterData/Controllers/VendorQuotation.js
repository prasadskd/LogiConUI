﻿angular.module('LogiCon').controller('VendorQuotationCntrl', ['$scope', '$uibModal', 'MerchantProfileService', 'limitToFilter', 'CustomerQuotationService', '$location', '$stateParams', 'growlService', 'Utility',
    function ($scope, $uibModal, MerchantProfileService, limitToFilter, CustomerQuotationService, $location, $stateParams, growlService, Utility) {
        $scope.tabs = [
         { title: 'General', content: 'Js/MasterData/Templates/VendorQuotation/general.html?v=' + Utility.Version, active: true, disabled: false },
         { title: 'Details', content: 'Js/MasterData/Templates/VendorQuotation/details.html?v=' + Utility.Version, active: false, disabled: false }
        ];

        $scope.isFrmVendorQuotationValid = false;
        $scope.$watch('frmVendorQuotation.$valid', function (valid) {
            $scope.isFrmVendorQuotationValid = valid;
        });
        $scope.cq = {
            QuotationItems: new Array()
        };

        $scope.AddCQDetails = function (index) {
            var modalInstance = $uibModal.open({
                animation: true,
                template: '<quotation-detail data="data" quotation="customer"></quotation-detail>',
                controller: 'QuotationDetailCntrl',
                windowClass: 'app-modal-window2',
                resolve: {
                    quotationDetailsObj: function () {
                        return (index != -1 ? $scope.cq.QuotationItems[index] : { Index: index });
                    }
                }
            });

            modalInstance.result.then(function (quotationDetails) {
                if ($scope.cq.QuotationItems != null) {
                    if (quotationDetails.Index != -1) {
                        $scope.cq.QuotationItems[quotationDetails.Index] = quotationDetails;
                    }
                    else {
                        $scope.cq.QuotationItems.push(quotationDetails);
                    }
                }
                else {
                    $scope.cq.QuotationItems = new Array();
                    $scope.cq.QuotationItems.push(quotationDetails);
                }
            }, function () {

            });
        };

        $scope.CustomerResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (response) {
                return limitToFilter(response.data, 15);
            }, function (err) { });
        };

        $scope.SaveVendorQuotation = function (cq) {
            if ($scope.isFrmVendorQuotationValid) {
                CustomerQuotationService.SaveCustomerQuotation(cq).then(function (d) {
                    growlService.growl(d.data, 'success');
                    $location.path('/MasterData/vendorquotation/list');
                }, function (err) { });
            } else {
                growlService.growl('Please enter all mandatory fields..', 'danger');
            }
        };

        $scope.getCustomerCode = function () {
            return $scope.cq.CustomerCode;
        };

        $scope.searchClick = function (type) {
            $stateParams.code = $scope.cq[type];
        };

        $scope.AddQuotation = function (quotationNo) {
            $location.path('vendorquotation/' + quotationNo);
        }

        $scope.DeleteQuotation = function (quotationNo) {
            CustomerQuotationService.DeleteQuotation(quotationNo).then(function (d) {
                
                growlService.growl('Deleted successfully', 'success');
                $scope.CustomerQuotationList();
            }, function (err) { });
        };

        var quotationNo = $stateParams.quotation;
        if (typeof quotationNo != 'undefined' && quotationNo != 'NEW' && quotationNo != '') {
            CustomerQuotationService.getCustomerQuotation(quotationNo).then(function (d) {
                $scope.cq = d.data;
            }, function (err) { });
        }
        else {
            CustomerQuotationService.getCustomerQuotationListByType(3862).then(function (d) {
                $scope.quotationlist = d.data;                
            }, function () { });
        }
    }]);
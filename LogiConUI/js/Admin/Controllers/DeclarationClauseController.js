﻿angular.module('LogiCon').controller('DeclarationClauseController', ['$scope', 'ClauseService', 'growlService', function ($scope, ClauseService, growlService) {


    $scope.init = function () {
        $scope.clause = {};
        $scope.lookUpData = {};
        $scope.GetLookUpdata();
    };

    $scope.GetLookUpdata = function () {
        ClauseService.GetLookupData().then(function (d) {
            $scope.lookUpData = d.data;
        }, function (err) {
            console.log(err);
        });
    };

    $scope.GetByClauseCode = function (code) {
        ClauseService.GetByClauseCode(code).then(function (d) {
            $scope.clause = d.data;
        }, function (err) {
            console.log(err);
        });
    };

    $scope.SaveClause = function (clause) {
        ClauseService.SaveClause(clause).then(function (d) {
            growlService.growl('Declaration Saved Successfully..', 'success');
        }, function (err) {
        });
    };

    $scope.init();
}]);
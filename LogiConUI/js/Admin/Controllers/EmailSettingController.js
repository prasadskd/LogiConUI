﻿angular.module('LogiCon').controller('EmailSettingController', ['$scope', 'EmailSettingService', 'growlService', function ($scope, EmailSettingService, growlService) {
    $scope.e = {};
    $scope.SaveEmailSetting = function (e) {        
        EmailSettingService.SaveEmailSetting(e).then(function (d) {
            growlService.growl('Success', 'success');
        }, function (err) { });
    };

    $scope.GetEmailSettings = function () {
        EmailSettingService.GetEmailSetting().then(function (d) {
            $scope.e = d.data;
        }, function (err) { });
    };

    $scope.GetEmailSettings();
}]);
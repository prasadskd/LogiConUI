﻿
app.constant('Utility', {
    BaseUrl: window.location.host.indexOf('localhost') != -1 ? 'http://localhost/LogiCon' : (window.location.host.indexOf('http://ragsarma-001-site10.htempurl.com') != -1 ? 'http://ragsarma-001-site12.htempurl.com' : 'http://ragsarma-001-site19.htempurl.com'),
    ServiceUrl: window.location.host.indexOf('localhost') != -1 ? 'http://localhost/LogiCon/api' : (window.location.host.indexOf('http://ragsarma-001-site10.htempurl.com') != -1 ? 'http://ragsarma-001-site12.htempurl.com/api' : 'http://ragsarma-001-site19.htempurl.com/api'),
    NetTmsUrl: 'http://localhost/NetTms.Api/api',
    CreatedBy: 'DEFAULTUSER',
    ModifiedBy: 'DEFAULTUSER',
    ReportPath: 'http://localhost/LogiCon/Report',
    Version: version,
    appVersion: '0.0.23'
});

//app.constant('Utility', {
//    BaseUrl: 'http://localhost:50766/',
//    ServiceUrl: 'http://localhost:50766/api',
//    NetTmsUrl: 'http://localhost:50766/api',
//    ReportPath: 'http://localhost:50766/Report',
//    CreatedBy: 'DEFAULTUSER',
//    ModifiedBy: 'DEFAULTUSER',
//    Version: version,
//    appVersion: '0.0.3'
//});


//app.constant('Utility', {
//    BaseUrl: 'http://ragsarma-001-site19.htempurl.com/',
//    ServiceUrl: 'http://ragsarma-001-site19.htempurl.com/api',
//    NetTmsUrl: 'http://ragsarma-001-site19.htempurl.com/api',
//    ReportPath: 'http://ragsarma-001-site19.htempurl.com/report',
//    CreatedBy: 'defaultuser',
//    ModifiedBy: 'defaultuser',
//    Version: version,
//    appVersion: '0.0.23'
//});

//app.constant('Utility', {
//    BaseUrl: 'http://uat.1trade.exchange/1tradeapi',
//    ServiceUrl: 'http://uat.1trade.exchange/1tradeapi/api',
//    NetTmsUrl: 'http://uat.1trade.exchange/1tradeapi/NetTms.Api/api',
//    ReportPath: 'http://uat.1trade.exchange/1tradeapi/Report',
//    CreatedBy: 'DEFAULTUSER',
//    ModifiedBy: 'DEFAULTUSER',
//    Version: version,
//    appVersion: '0.0.41'
//});
